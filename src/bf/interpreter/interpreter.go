package interpreter

import (
	"fmt"
	"io/ioutil"
)

// BF スクリプト情報
type BF struct {
	buffer []byte
	index  int
	output []byte
}

// SymbolType シンボルタイプ
type SymbolType int

// OPコード
const (
	PointerIncrement SymbolType = iota
	PointerDecrement
	ValueIncrement
	ValueDecrement
	Print
	LoopStart
	LoopEnd
)

// Symbol シンボル
type Symbol struct {
	symbolType SymbolType // シンボルタイプ
	block      []Symbol   // ブロック([]用)
}

// Run スクリプトの実行
func (b *BF) Run(sourceFile string) {
	// 初期化
	b.buffer = make([]byte, 1000)
	b.index = 0

	// ファイル読み込み
	file, err := ioutil.ReadFile(sourceFile)
	if err != nil {
		fmt.Println("File open error. ", sourceFile)
		return
	}
	// 実行
	symbols, _ := b.parse(string(file), 0, false)
	b.exec(symbols)
	fmt.Println(string(b.output))
}

// parse ソースの解析
func (b *BF) parse(file string, offset int, isLoop bool) ([]Symbol, int) {
	var symbolList []Symbol
	var fileLength = len(file)
	for {
		if fileLength <= offset {
			// ファイル終了
			break
		}
		var symbol Symbol

		if b.isSymbol(file, ">", offset) {
			offset += len(">")
			symbol.symbolType = PointerIncrement
		} else if b.isSymbol(file, "<", offset) {
			offset += len("<")
			symbol.symbolType = PointerDecrement
		} else if b.isSymbol(file, "+", offset) {
			offset += len("+")
			symbol.symbolType = ValueIncrement
		} else if b.isSymbol(file, "-", offset) {
			offset += len("-")
			symbol.symbolType = ValueDecrement
		} else if b.isSymbol(file, ".", offset) {
			offset += len(".")
			symbol.symbolType = Print
		} else if b.isSymbol(file, "[", offset) {
			offset += len("[")
			block, ofs := b.parse(file, offset, true)
			offset = ofs
			symbol.symbolType = LoopStart
			symbol.block = block
		} else if b.isSymbol(file, "]", offset) {
			symbol.symbolType = LoopEnd
			offset += len("]")
			if isLoop {
				return symbolList, offset
			}
		} else {
			offset++
			continue
		}

		symbolList = append(symbolList, symbol)
	}

	return symbolList, offset
}

// isSymbol シンボル判定
func (b *BF) isSymbol(file string, name string, offset int) bool {
	if len(file) < offset+len(name) {
		return false
	}
	if file[offset:offset+len(name)] == name {
		return true
	}
	return false
}

// exec ATSの実行
func (b *BF) exec(symbols []Symbol) {
	for _, symbol := range symbols {
		if symbol.symbolType == PointerIncrement {
			b.index++
		} else if symbol.symbolType == PointerDecrement {
			if b.index == 0 {
				fmt.Println("Pointer decrement error")
				return
			}
			b.index--
		} else if symbol.symbolType == ValueIncrement {
			b.buffer[b.index]++
		} else if symbol.symbolType == ValueDecrement {
			if b.buffer[b.index] == 0 {
				fmt.Println("Value decrement error")
				return
			}
			b.buffer[b.index]--
		} else if symbol.symbolType == Print {
			b.output = append(b.output, b.buffer[b.index])
		} else if symbol.symbolType == LoopStart {
			for b.buffer[b.index] > 0 {
				b.exec(symbol.block)
			}
		}
	}
	return
}
