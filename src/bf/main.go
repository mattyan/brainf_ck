package main

import (
	"bf/interpreter"
	"flag"
	"fmt"
)

// APPVERSION アプリバージョン
const APPVERSION = "0.1"

func main() {
	var versionFlag bool
	flag.BoolVar(&versionFlag, "v", false, "Print the version number.")
	flag.Parse()

	if versionFlag {
		// バージョン情報
		fmt.Println("brainfuck interpreter Version:", APPVERSION)
	}

	if flag.NArg() > 0 {
		var interpreter = new(interpreter.BF)
		interpreter.Run(flag.Arg(0))
	}
}
