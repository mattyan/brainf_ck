package main

import (
	"flag"
	"fmt"
)

func main() {
	var versionFlag bool
	flag.BoolVar(&versionFlag, "v", false, "Print the version number.")
	flag.Parse()

	if versionFlag {
		// バージョン情報
		fmt.Println("brainfuck interpreter Version:", "0.1")
	}

	var message []byte
	if flag.NArg() > 0 {
		message = []byte(flag.Arg(0))
	}
	var bfsource string
	// 文字列解析
	for _, c := range message {
		var num int
		num = int(c)

		var fact1 = 0
		var fact2 = 0
		var fact3 = 255

		// 因数分解
		for p := num; p > 1; p-- {
			var q = int(num / p)
			var r = num % p
			if p < q {
				break
			}
			fact1 = p
			fact2 = q
			fact3 = r
		}
		// ソース作成
		bfsource += ">"
		for i := 0; i < fact1; i++ {
			bfsource += "+"
		}
		bfsource += "[<"
		for i := 0; i < fact2; i++ {
			bfsource += "+"
		}
		bfsource += ">-]<"
		for i := 0; i < fact3; i++ {
			bfsource += "+"
		}
		bfsource += ".>"
	}
	fmt.Println(bfsource)
}
